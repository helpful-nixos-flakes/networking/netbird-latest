{
  description = "Flake to create the packages the latest version of Netbird.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages = rec {
          netbird-latest = pkgs.callPackage ./default.nix { };
          default = netbird-latest;
        };
      }
    );
}