{ 
  stdenv,
  lib,
  nixosTests,
  nix-update-script,
  buildGoModule,
  fetchFromGitHub,
  installShellFiles
}:

let
  modules = {
    client = "netbird";
    management = "netbird-mgmt";
    signal = "netbird-signal";
  };
in buildGoModule rec {
  pname = "netbird";
  version = "0.28.3";

  src = fetchFromGitHub {
    owner = "netbirdio";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-ue5vMZNLeDAtCa/BzX4aMEcmnxcN41hbmccME8I6VYk=";
  };

  vendorHash = "sha256-/RfEjTB2n9URhcagasN6yYaejLviv7kHodPo0Y+k3mo=";

  nativeBuildInputs = [ installShellFiles ];

  subPackages = lib.attrNames modules;

  ldflags = [
    "-s"
    "-w"
    "-X github.com/netbirdio/netbird/version.version=${version}"
    "-X main.builtBy=nix"
  ];

  # needs network access
  doCheck = false;

  postPatch = ''
    # make it compatible with systemd's RuntimeDirectory
    substituteInPlace client/cmd/root.go --replace-fail 'unix:///var/run/netbird.sock' 'unix:///var/run/netbird/sock'
    substituteInPlace client/ui/client_ui.go --replace-fail 'unix:///var/run/netbird.sock' 'unix:///var/run/netbird/sock'
  '';

  postInstall = lib.concatStringsSep
    "\n"
    (lib.mapAttrsToList
      (module: binary: "mv $out/bin/${lib.last (lib.splitString "/" module)} $out/bin/${binary}")
      modules);

  passthru = {
    tests.netbird = nixosTests.netbird;
    updateScript = nix-update-script { };
  };

  meta = with lib; {
    homepage = "https://netbird.io";
    changelog = "https://github.com/netbirdio/netbird/releases/tag/v${version}";
    description = "Connect your devices into a single secure private WireGuard®-based mesh network with SSO/MFA and simple access controls";
    license = licenses.bsd3;
    mainProgram = "netbird";
  };
}